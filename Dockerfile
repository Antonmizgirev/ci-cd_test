FROM node:12.1-alpine
MAINTAINER Anton Mizgirev (anton.mizgirev@senacor.com)
WORKDIR /usr/src/app

COPY app.js ./
EXPOSE 3000

CMD [ "node", "app.js" ]